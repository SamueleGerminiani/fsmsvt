`timescale 1ns/1ns

// Testbench
module test;
  reg  clk, a,rst;
  wire out1, out2,out3;

  // Instantiate device under test
  fsm FSM(.rst(rst),.clk(clk),
          .a(a),
          .out1(out1),
          .out2(out2),
          .out3(out3));


  always
    begin
      #5 clk =~clk;
    end

  initial begin
    $dumpfile("fsm.vcd");
    $dumpvars(0,test);

    clk = 0;
    rst = 1;
    a = 0;
    #5;
    rst = 0;
    a = 1;
    #5;
    a = 0;
    #10;
    a = 1;
    #60
    a = 0;
    #20;
    $finish;
  end
endmodule

