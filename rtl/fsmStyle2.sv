/*
 * Finite state machine.
 */ 

module fsm(input clk,rst, a, output out1, out2, out3);

  // State encodings
  typedef enum reg[1:0] {START=0,STATE_1,STATE_2,FINAL} State;

  State currState;
  State nextState;
    
  always_ff @(posedge clk or rst) begin
      if(rst)begin
          currState<=START;
      end else
          currState<=nextState;
  end
  
  // State transitions
  always_comb begin
    case (currState)
      START:
        if (a) begin
          nextState = STATE_1;
        end else begin
          nextState = START;
        end
      STATE_1:
        if (a) begin
          nextState = STATE_2;
        end else begin
          nextState = START;
        end
      STATE_2:
        if (a) begin
          nextState = FINAL;
        end else begin
          nextState = START;
        end
      FINAL:
        if (a) begin
          nextState = FINAL;
        end else begin
          nextState = START;
        end
default:
        nextState = START;
    endcase
  end

  //output logic
  assign out1 = (currState == STATE_1);
  assign out2 = (currState == STATE_2);
  assign out3 = (currState == FINAL);
endmodule
