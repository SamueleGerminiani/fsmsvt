rm -rf work
vlib work
vlog -sv rtl/fsmStyle2.sv tb/tb.sv
vsim -voptargs=+acc -c work.test -do "run 1ms; exit"
gtkwave fsm.vcd sfsmS2.gtkw
