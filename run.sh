rm -rf work
vlib work
vlog -sv rtl/fsm.sv tb/tb.sv
vsim -voptargs=+acc -c work.test -do "run 1ms; exit"
gtkwave fsm.vcd sfsm.gtkw
